package br.com.itau;

import br.com.itau.modelo.Lancamento;
import br.com.itau.service.LancamentoService;

import java.util.List;

import static java.util.Comparator.comparing;

public class Exercicio {

    public static void main(String[] args) {

        List<Lancamento> lancamentos = new LancamentoService().listarLancamentos();

        System.out.println("Gastos ordenados por meses: ");
        lancamentos.stream()
                .sorted(comparing(Lancamento::getMes))
                .forEach(valor -> System.out.println(valor.getValor()));

        System.out.println("\n");

        System.out.println("Todos os lancamentos da categoria 4: ");
        lancamentos.stream()
                .sorted(comparing(Lancamento::getCategoria)
                        .reversed());
        System.out.println(lancamentos.get(7).getValor());

        System.out.println("\n");

        System.out.println("Total da fatura do mes 2: ");
        lancamentos.stream()
                .sorted(comparing(Lancamento::getMes)
                        .reversed());
        System.out.println(lancamentos.get(1).getValor());


    }

}
